import cv2
import numpy as np
import math
from matplotlib import pyplot as plt
from mpl_toolkits import mplot3d


#Pads out image by adding edge values and returns new image
def padding(img):
    img = np.pad(img, ((1, 1), (1, 1)), 'edge')
    return img

#Applies kernel to an image and returns new image
def kernel(img, kernel):
    img2 = np.zeros(img.shape)
    img = padding(img)
    for i in range(1, img.shape[0] - 1):
        for j in range(1, img.shape[1] - 1):
            imgsect = img[i - 1:i + 2, j - 1:j + 2]
            img2[i - 1, j - 1] = pixel(imgsect, kernel)
            if img2[i - 1, j - 1] == 0:
                img2[i - 1, j - 1] = 0.1
    return img2

#Applies kernel to 3x3 grid
def pixel(img, kernel):
    pixel = 0
    for i in range(0, 3):
        for j in range(0, 3):
            pixel = pixel + img[i, j] * kernel[i, j]
    return pixel

#thresholds the gradient image and supplies it to the hough line and hough circle
def largeGradients(img, quota):
    num = 0
    img3 = np.zeros(img.shape)
    list2 = []
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if img[i, j] > quota:
                num = num + 1
                img3[i, j] = 255
                list2.append((i, j))
    if num>1100:
        img3, A,list4 = largeGradients(img,quota+20)
    else:
        A = votecircles(list2, max(img.shape[0], img.shape[1]))
        list4 = votelines(list2)
    return img3, A, list4

#Creates the hough space for circles and displays hough space
def votecircles(list2, shape):
    A = np.zeros((250, 250, int(shape/2)))
    for pixel in list2:
        for r in range(20, int(shape*1/2),2):
            for t in range(0, 360,4):
                t = t * math.pi / 180
                b = int((pixel[0] - r * math.sin(t)) / 8)
                a = int((pixel[1] - r * math.cos(t)) / 8)
                A[a, b, int(r / 8)] += 1
    ax = plt.axes(projection='3d')
    xs=[]
    ys=[]
    zs=[]
    col=[]
    for i in range(0,150):
        for j in range(0,150):
            for k in range(0,int(shape/2)):
                if A[i,j,k]>0:
                    xs.append(i*8)
                    ys.append(j*8)
                    zs.append(k*8)
                    col.append(A[i,j,k])
    ax.scatter3D(xs, ys, zs, c=col, cmap='Reds');
    plt.show()
    return A

#Creates the hough space for lines and displays hough space
def votelines(list2):
    list4 = []
    B = np.zeros((700, 360))
    for pixel in list2:
        for t in range(0, 360):
            r = int((pixel[1] * math.cos(t * math.pi / 180) + pixel[0] * math.sin((t * math.pi / 180))) / 2)
            B[r, int(t / 2)] += 1
            if B[r, int(t / 2)] > 80:
                list4.append((r, t/2))
    fig, ax = plt.subplots()
    ax.imshow(B)
    plt.show()
    list4 = list(dict.fromkeys(list4))
    return list4

#Finds the intersection of 2 lines in cartesian form
def lineintersect(m1,c1,m2,c2):
    x = (c1 - c2)/(m2-m1)
    y = m1 *x +c1
    return x,y

#Calculates the intersection of all lines with a large gradient difference
def hasIntersection(lines, A, img):
    max1 = []
    for line1 in lines:
        for line2 in lines:
            if  line1[0]!=line2[0] and abs(line1[0] - line2[0]) > 0.5:
                x,y = lineintersect(line1[0],line1[1],line2[0],line2[1])
                if 0<x<img.shape[1] and 0<y<img.shape[0]:
                    r =np.argmax(A[int(x/8),int(y/8),:])
                    max1.append((int(x/8), int(y/8), r))
    max1=mergecircles(max1)
    max1 = list(dict.fromkeys(max1))
    return max1

#Calculates if 2 circles are similar
def similar(circle1,circle2):
    if abs(circle1[0] - circle2[0]):
        if abs(circle1[1] - circle2[1]):
            return max(circle1[2], circle2[2])
    return 0

#Merges all similar circles found in an image
def mergecircles(circles):
    if len(circles) == 0:
        return []
    merged = [circles[0]]
    for circle in circles:
        found=False
        for i in range(0,len(merged)):
            if similar(merged[i],circle)>0 and found==False:
                circle2 =merged[i]
                x = (circle[0] + circle2[0])/2
                y = (circle[0] + circle2[0]) / 2
                r = (circle[2]+ circle2[2])/2
                merged[i] = (x,y,r)
                found = True
        if found==False:
            merged.append(circle)
    return merged

#Main function runs the hough transform and returns all instances of found dartboard and displays relevant images
def hough(img):
    img = padding(img)
    kernelx = np.array([[1, 0, -1], [2, 0, -2], [1, 0, -1]])
    kernely = np.array([[1, 2, 1], [0, 0, 0], [-1, -2, -1]])
    dy = kernel(img, kernely)
    dx = kernel(img, kernelx)

    gradient = (abs(dx) + abs(dy))
    max_grad = gradient.max()
    img3, A, lines = largeGradients(gradient, max_grad*0.5)
    fig, ax = plt.subplots()
    cartlines = []
    for line in lines:
        angle = line[1] * 2 * math.pi / 180
        if math.tan(angle) != 0:
            m = -1 / math.tan(angle)
        else:
            m = 1000
        x1 = math.cos(angle) * line[0] * 2
        y1 = math.sin(angle) * line[0] * 2
        c = -m * x1 + y1
        x = np.linspace(0, 600, 300)
        y = m * x
        y = [ x+c for x in y]
        cartlines.append((m,c))
        plt.plot(x, y)
    ax.imshow(img)
    fig2, ax2 = plt.subplots()
    ax2.imshow(img3)
    fig3, ax3 = plt.subplots()
    dartboards = []
    circles = hasIntersection(cartlines,A,img)
    for circle in circles:
        print(circle)
        x = circle[0]*8
        y=circle[1]*8
        r = circle[2]*8
        circle1 = plt.Circle((x, y), r, color='r', fill=False)
        ax.add_artist(circle1)
        dartboards.append((x-r,y-r,r*2,r*2))
    plt.show()
    print(dartboards)
    return dartboards
