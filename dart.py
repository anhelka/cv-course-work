
import numpy as np
from sklearn.decomposition import PCA
np.seterr(divide='ignore', invalid='ignore')
import cv2
import random
import time 
import warnings
from hough import hough
warnings.filterwarnings('ignore', "Intel MKL ERROR")


sample = ["dart0.jpg", "dart1.jpg", "dart2.jpg", "dart3.jpg", "dart4.jpg", "dart5.jpg", "dart6.jpg", "dart7.jpg", "dart8.jpg", "dart9.jpg", "dart10.jpg", "dart11.jpg", "dart12.jpg", "dart13.jpg", "dart14.jpg", "dart15.jpg"]
groundTruth = [[(457,35,130,115)],[(213,170,165,126)],[(96,95,99,96)],[(331,158,56,55)],[(172,101,184,183)],[(436,149,86,96)],[(210,109,64,64)],[(253,177,119,134)],[(838,229,124,109),(65,256,68,91)],[(213,71,217,192)],[(86,109,105,106),(582,132,62,81),(917,147,37,73)],[(172,100,68,61)],[(157,77,66,145)],[(269,124,140,125)],[(117,107,126,119),(986,106,126,113)],[(160,68,126,121)]]

cascade = cv2.CascadeClassifier('cascade.xml')
def combinehough(houghfaces, faces):
    combine = []
    for face in faces:
        found = False
        for hough in houghfaces:
            if found==False:
                (x1, y1, w1, h1) = face
                (x2, y2, w2, h2) = hough

                left = np.maximum(x1, x2)
                right = np.minimum(x1 + w1, x2 + w2)
                top = np.maximum(y1, y2)
                bottom = np.minimum(y1 + h1, y2 + h2)

                if not(left > right or bottom<top):
                    intersec = (right - left) * (bottom - top)
                    if intersec>0:
                        found=True
                        combine.append(face)
    return combine

def detect( img_file, counter ):
    # Load input image in grayscale mode
    img = cv2.imread(img_file)
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # Blur image using median blur 
    median = cv2.medianBlur(gray_img,5)
    edges = cv2.Canny(median,100,200)
    # TODO: 
    #   Run ellipse detection on blurred image
    #   Run line detection on unblurred image 

    # Detect dartboard faces using our cascade 
    faces = cascade.detectMultiScale( gray_img, 1.1, 1)
    houghfaces = hough(gray_img)
    for (x,y,w,h) in faces:
        roi = gray_img[y:y+h, x:x+w]
        findEllipse(roi, w, h, edges, x, y)
    faces= combinehough(houghfaces,faces)
    #Display result and groundtruth
    display(img, faces, counter)

    #Calculate TPR and F1 Score
    calculateCorrectness(faces, groundTruth[counter])

    # cv2.imshow('img', edges)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def display( img, faces, counter ):
    # Take original image, draw predicted dartboards in green and ground truths in red
    for x,y,w,h in faces:
        print(x)
        print(y)
        print(w)
        print(h)
        img = cv2.rectangle(img,(int(x),int(y)),(int(x+w),int(y+h)),(0,255,0),2)

    for (x,y,w,h) in groundTruth[counter]:
        img = cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)

    print(counter + 1 , " - Faces found: ", len(faces), "Expected: ", len(groundTruth[counter]))

    # Show as image file
    cv2.imshow('img', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def calculateCorrectness( positives, groundTruths ):
    tpos = 0
    for actualFace in groundTruths:
        found = False
        for face in positives:
            result = intersectionOverUnion( face, actualFace )
            if result > 0:
                found=True
        if found==True:
            tpos+=1
    nrPositives = len(positives)
    falseNegative = len(groundTruths) - tpos

    f1 = f1Score(tpos, nrPositives, falseNegative)
    if (tpos+falseNegative) == 0:
        tpr =0
    else:
        tpr = tpos / (tpos+falseNegative)

    print("True Positive Rate: ", tpr, ", F1 Score: ", f1)


def intersectionOverUnion( box1, box2 ):
    (x1, y1, w1, h1) = box1
    (x2, y2, w2, h2) = box2

    left = np.maximum(x1, x2)
    right = np.minimum(x1+w1, x2+w2)
    top = np.maximum(y1, y2)
    bottom = np.minimum(y1+h1, y2+h2)

    if left > right:
        intersec = 0
    elif bottom < top:
        intersec = 0
    else:
        intersec = (right-left) * (bottom-top)

    # Calculate the area of union
    union = w1 * h1 + w2 * h2 - intersec

    # Calculate the IOU/Jaccard Index
    jaccard = intersec / union

    if jaccard < 0.4:
        return 0
    else:
        return jaccard

def f1Score(tpos, positives, falseNegative):
    if tpos > 0:
        precision = tpos/positives
        recall = tpos/(tpos+falseNegative)
        f1Score = 2*(recall*precision)/(recall+precision)
        return f1Score
    else:
        return 0

def findEllipse( img, width, height, edges_img, original_x, original_y ): 
    accumulator = []
    start = time.time()

    # Find potential ellipse 
    # While we find ellipses or max epoch reached
    #   for a fixed nr of iterations
    #       if ellipse is similar to an ellipse in the accumulator 
    #           average the two ellipses 
    #           add new ellipse to accumulator 
    #           replace the one in the accumulator 
    #           add 1 to score
    #       else add ellipse to empty position in accumulator with score of 1
    # Select ellipse with highest score and save to best ellipse table
    while (time.time() - start) < 20: 
        xs = generate3RandomInt(0,width-1)
        ys = generate3RandomInt(0,height-1)
        lines = []
        for i in range(3):
            roi = img[ys[i]-3:ys[i]+3, xs[i]-3:xs[i]+3]
            points = []
            for m in range(len(roi)):
                for n in range(len(roi[m])):
                    if roi[m][n] == 255:
                        points.append((n,m))
            if len(points) > 1:
                x,y = zip(*points)
                if sameElements(x):
                    lines.append((np.polyfit(x, y, 1), n, m))
        if len(lines) == 3:
            intersects = []
            line, x, y = zip(*lines)
            if line[0][0] != line[1][0]:
                intersects.append(findIntersect(line[0], line[1]))
            if line[1][0] != line[2][0]:
                intersects.append(findIntersect(line[1], line[2]))    
            if len(intersects) == 2:
                bisectors = []
                # Calculate Bisector 
                bisectors.append(calculateBisector((x[0],y[0]),(x[1],y[1]),intersects[0]))
                bisectors.append(calculateBisector((x[1],y[1]),(x[2],y[2]),intersects[1]))
                # print("bisectors: ", bisectors)
                if bisectors[0][0] != bisectors[1][0]:
                    (p, q) = findIntersect(bisectors[0],bisectors[1])
                    print(p,q)
                    p, q = int(p), int(q)
                    A, B, C = solveABC(xs, ys)
                    semimajor = int(np.sqrt(np.absolute(A**(-1))))
                    semiminor = int(np.sqrt(np.absolute(C**(-1))))
                    # print("semimajor: ", semimajor, "semininor: ", semiminor)
                    if (4*A*C-B**2) > 0 and semimajor > 0 and semiminor > 0:
                        # Calculate pixel to circumference ratio 
                        # Add ellipse to accumulator 
                        # ratio = pixel2circumference(edges_img, (original_x+p,original_y+q,semimajor,semiminor))
                        # if ratio > 0.1:
                        # Check with ellipses in accumulator before adding a new one 
                        if len(accumulator) == 0:
                            accumulator.append((p, q, semimajor, semiminor, 1))
                        else: 
                            for (pi,qi,semimajori,semiminori) in accumulator:
                                print(accumulator)

def pixel2circumference( img, ellipse ):
    # Image -> Mask -> Float 
    centre_x, centre_y, semimajor, semiminor = ellipse
    print(ellipse)
    height,width = img.shape
    ellipse_img = np.zeros((height,width), np.uint8)
    cv2.ellipse( ellipse_img, (centre_x, centre_y), (semimajor, semiminor), 0, 0, 360, color=1, thickness=1)
    masked_data = cv2.bitwise_and(img, img, mask=ellipse_img)
    cv2.imshow("masked", masked_data)
    cv2.waitKey(0)   

    circumference = np.pi*semimajor*semiminor 
    pixels = np.count_nonzero(masked_data == 255)
    return pixels/circumference

def solveABC( xs, ys ):
    X = np.array([[xs[0]**2, 2*xs[0]*ys[0], ys[0]**2], [xs[1]**2, 2*xs[1]*ys[1], ys[1]**2], [xs[2]**2, 2*xs[2]*ys[2], ys[2]**2]])
    Y = np.array([1,1,1])
    A,B,C = np.linalg.inv(X).dot(Y)
    return A, B, C

def sameElements( givenList ):
    if givenList.count(givenList[0]) == len(givenList):
        return False 
    else:
        return True 

def findIntersect( line1, line2 ):
    # [Int, Int] -> [Int, Int] -> [Int, Int]
    x = (line2[1]-line1[1])/(line1[0]-line2[0])
    y = x*line1[0]+line1[1]
    return (x,y)

def calculateBisector(x1, x2, intersection):
    midpoint = ((x1[0]-x2[0])/2,(x1[1]-x2[1])/2)
    slope = (midpoint[1]-intersection[1])/(midpoint[0]-intersection[0])
    b = slope*intersection[0]-intersection[1]
    return (slope, b)

def generate3RandomInt( low, high ):
    x = random.randint(low, high)
    y = random.randint(low, high)
    z = random.randint(low, high)
    return x,y,z

def main():
    counter = 0
    for img_file in sample:
        detect( img_file, counter )
        counter += 1
main()
